package com.functionfocus.coviddemo.sources.cases.models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * An object Model of the API response loaded by the ActiveCaseDataExtractor
 *
 * @author matthew
 */
public final class CaseCountModel {

    /**
     * This URL String of the host that provides the data modeled by this class.
     */
    public static final String DATA_SOURCE_PATH = "https://covid-19.dataflowkit.com/v1";

    @JsonProperty("Active Cases_text")
    private String activeCasesText;

    @JsonProperty("Country_text")
    private String countryText;

    @JsonProperty("Last Update")
    private String lastUpdateDateText;

    @JsonProperty("New Cases_text")
    private String newCasesText;

    @JsonProperty("New Deaths_text")
    private String newDeathsText;

    @JsonProperty("Total Cases_text")
    private String totalCasesText;

    @JsonProperty("Total Deaths_text")
    private String totalDeathsText;

    @JsonProperty("Total Recovered_text")
    private String totalRecoveredText;

    public CaseCountModel() {

    }

    protected int toInt(String s) {
        int value = 0;
        try {
            value = Integer.parseInt(s.replaceAll("[^\\d]", ""));
        } catch (Exception ignoredException) {
            // ignore
        }
        return value;
    }

    public int getActiveCases() {
        return toInt(this.getActiveCasesText());
    }

    /**
     * @return the activeCasesText
     */
    public String getActiveCasesText() {
        return activeCasesText;
    }

    /**
     * @param activeCasesText the activeCasesText to set
     */
    public void setActiveCasesText(String activeCasesText) {
        this.activeCasesText = activeCasesText;
    }

    /**
     * @return the countryText
     */
    public String getCountryText() {
        return countryText;
    }

    /**
     * @param countryText the countryText to set
     */
    public void setCountryText(String countryText) {
        this.countryText = countryText;
    }

    /**
     * @return the lastUpdateDateText
     */
    public String getLastUpdateDateText() {
        return lastUpdateDateText;
    }

    /**
     * @param lastUpdateDateText the lastUpdateDateText to set
     */
    public void setLastUpdateDateText(String lastUpdateDateText) {
        this.lastUpdateDateText = lastUpdateDateText;
    }

    /**
     * @return the newCasesText
     */
    public String getNewCasesText() {
        return newCasesText;
    }

    /**
     * @param newCasesText the newCasesText to set
     */
    public void setNewCasesText(String newCasesText) {
        this.newCasesText = newCasesText;
    }

    /**
     * @return the newDeathsText
     */
    public String getNewDeathsText() {
        return newDeathsText;
    }

    /**
     * @param newDeathsText the newDeathsText to set
     */
    public void setNewDeathsText(String newDeathsText) {
        this.newDeathsText = newDeathsText;
    }

    /**
     * @return the totalCasesText
     */
    public String getTotalCasesText() {
        return totalCasesText;
    }

    /**
     * @param totalCasesText the totalCasesText to set
     */
    public void setTotalCasesText(String totalCasesText) {
        this.totalCasesText = totalCasesText;
    }

    /**
     * @return the totalDeathsText
     */
    public String getTotalDeathsText() {
        return totalDeathsText;
    }

    /**
     * @param totalDeathsText the totalDeathsText to set
     */
    public void setTotalDeathsText(String totalDeathsText) {
        this.totalDeathsText = totalDeathsText;
    }

    /**
     * @return the totalRecoveredText
     */
    public String getTotalRecoveredText() {
        return totalRecoveredText;
    }

    /**
     * @param totalRecoveredText the totalRecoveredText to set
     */
    public void setTotalRecoveredText(String totalRecoveredText) {
        this.totalRecoveredText = totalRecoveredText;
    }

}
