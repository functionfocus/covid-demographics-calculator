package com.functionfocus.coviddemo.sources.cases;

import com.functionfocus.coviddemo.RegionModel;
import com.functionfocus.coviddemo.sources.cases.models.CaseCountModel;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

/**
 * Retrieves and processes chase count data provided by the external provider
 *
 * @author matthew
 */
public final class ActiveCaseDataExtractor {

    private RegionModel regionModel;

    public ActiveCaseDataExtractor() {

    }

    public RegionModel retrieve() {
        this.setRegionModel(new RegionModel());
        Flux<CaseCountModel> response = WebClient.create().get()
                .uri(CaseCountModel.DATA_SOURCE_PATH)
                .retrieve()
                .bodyToFlux(CaseCountModel.class);
        response.toStream().forEachOrdered(model -> {
            this.getRegionModel().reportRegion(model.getCountryText(), model.getActiveCases());
            //System.out.println(model.getCountryText() + " has " + model.getActiveCasesText() + " cases.");
        });
        return regionModel;
    }

    /**
     * @return the regionModel
     */
    protected RegionModel getRegionModel() {
        return regionModel;
    }

    /**
     * @param regionModel the regionModel to set
     */
    protected void setRegionModel(RegionModel regionModel) {
        this.regionModel = regionModel;
    }

}
