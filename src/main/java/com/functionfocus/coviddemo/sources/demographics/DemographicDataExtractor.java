package com.functionfocus.coviddemo.sources.demographics;

import com.functionfocus.coviddemo.RegionModel;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author matthew
 */
public final class DemographicDataExtractor {

    private File file;
    private XSSFWorkbook workbook;
    private RegionModel dataModel;

    private static final String SHEET_NAME_DEMOGRAPHICS_DATA = "ESTIMATES";
    private static final String COLUMN_HEADING_REGION_NAME_SUBSTRING = "Region, subregion, country or area";
    private static final int ROW_NUMBER_DATA_HEADINGS = 16;
    /**
     * The workbook presents data in numbers of millions. Multiply the displayed
     * data by this factor to achieve the actual population
     */
    private static final int POPULATION_DATA_SHEET_DATA_MULTIPLIER = 1000;

    private DemographicDataExtractor() {
        super();
    }

    public DemographicDataExtractor(File file) {
        this();
        this.setFile(file);
        this.init();
    }

    protected void init() {
        if (this.getFile() != null) {
            try (FileInputStream fis = new FileInputStream(this.getFile())) {
                this.setWorkbook(new XSSFWorkbook(fis));
            } catch (IOException ex) {
                Logger.getLogger(DemographicDataExtractor.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Once initialized, call the retrieve() method to extract data from the
     * workbook
     *
     * @return
     */
    public RegionModel retrieve() {
        this.setDataModel(new RegionModel());
        XSSFSheet sheet = this.getDemographicsDataSheet();
        XSSFRow headingsRow = sheet.getRow(ROW_NUMBER_DATA_HEADINGS);
        short regionNameColumnIndex = this.getRegionNameColumnIndex();
        short currentYearDataColumnIndex = this.getCurrentYearDateColumnIndex();

        for (int i = headingsRow.getRowNum() + 1; i < sheet.getLastRowNum() + 1; i++) {
            XSSFRow row = sheet.getRow(i);
            XSSFCell regionNameCell = row.getCell(regionNameColumnIndex);
            XSSFCell currentYearDataCell = row.getCell(currentYearDataColumnIndex);
            //System.out.println(i);
            if (currentYearDataCell.getCellType().equals(CellType.NUMERIC)) {
                String regionName = regionNameCell == null ? "" : regionNameCell.getStringCellValue();
                BigDecimal regionPopulation = new BigDecimal(currentYearDataCell.getNumericCellValue());
                this.getDataModel().reportRegion(regionName, regionPopulation.intValue() * POPULATION_DATA_SHEET_DATA_MULTIPLIER);
            }
        }
        return this.getDataModel();
    }

    protected XSSFRow getDataHeadingsRow() {
        XSSFSheet sheet = this.getDemographicsDataSheet();
        XSSFRow row = sheet.getRow(ROW_NUMBER_DATA_HEADINGS);
        return row;
    }

    protected short getRegionNameColumnIndex() {
        short columnIndex = -1;
        XSSFRow row = this.getDataHeadingsRow();
        XSSFCell cell;
        for (short i = 0; i < row.getLastCellNum(); i++) {
            cell = row.getCell(i);
            if (cell != null) {
                String value = cell.getStringCellValue();
                if (value != null && value.contains(COLUMN_HEADING_REGION_NAME_SUBSTRING)) {
                    columnIndex = i;
                }
            }
        }

        return columnIndex;
    }

    protected short getCurrentYearDateColumnIndex() {
        XSSFRow row = getDataHeadingsRow();
        XSSFCell cell;
        String currentYear = getCurrentYearString();
        short columnIndex = -1;
        for (short i = 0; i < row.getLastCellNum(); i++) {
            cell = row.getCell(i);
            if (cell != null) {
                String value = cell.getStringCellValue();
                if (value != null && value.equals(currentYear)) {
                    columnIndex = i;
                }
            }
        }
        return columnIndex;
    }

    protected String getCurrentYearString() {
        return new SimpleDateFormat("yyyy").format(new Date());
    }

    protected XSSFSheet getDemographicsDataSheet() {
        return this.getWorkbook().getSheet(SHEET_NAME_DEMOGRAPHICS_DATA);
    }

    /**
     * @return the file
     */
    public File getFile() {
        return file;
    }

    /**
     * @param file the file to set
     */
    public void setFile(File file) {
        this.file = file;
    }

    /**
     * @return the workbook
     */
    protected XSSFWorkbook getWorkbook() {
        return workbook;
    }

    /**
     * @param workbook the workbook to set
     */
    protected void setWorkbook(XSSFWorkbook workbook) {
        this.workbook = workbook;
    }

    /**
     * @return the dataModel
     */
    protected RegionModel getDataModel() {
        return dataModel;
    }

    /**
     * @param dataModel the dataModel to set
     */
    protected void setDataModel(RegionModel dataModel) {
        this.dataModel = dataModel;
    }

}
