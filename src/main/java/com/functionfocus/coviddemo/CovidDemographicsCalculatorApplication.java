package com.functionfocus.coviddemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public  class CovidDemographicsCalculatorApplication {

    public static void main(String[] args) {
        SpringApplication.run(CovidDemographicsCalculatorApplication.class, args);
        
    }

    public CovidDemographicsCalculatorApplication() {
        CovidDemographicsCalculator calculator = new CovidDemographicsCalculator();
        calculator.calculate();
    }

}
