package com.functionfocus.coviddemo;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 *
 * @author matthew
 */
public final class RegionDetailModel {

    private String regionName;
    private long population;
    private long caseCount;

    public RegionDetailModel() {

    }

    public BigDecimal getCaseCountPopulationRatio() {
        return computeRatio(this.getCaseCount(), this.getPopulation());
    }
    
    public String getCaseCountRatioPer(long quantity){
        String s;
        BigDecimal perCapita = this.getCaseCountPopulationRatio();
        BigDecimal perDivisor = perCapita.multiply(BigDecimal.valueOf(quantity).setScale(5, RoundingMode.HALF_EVEN));
        return perDivisor.toPlainString();
    }

    protected BigDecimal computeRatio(long numerator, long denominator) {
        BigDecimal ratio = BigDecimal.ZERO;
        if (denominator != 0) {
            ratio = (BigDecimal.valueOf(numerator).setScale(10, RoundingMode.HALF_EVEN)).divide(BigDecimal.valueOf(denominator).setScale(10, RoundingMode.HALF_EVEN), RoundingMode.HALF_EVEN);
        }
        return ratio;
    }

    /**
     * @return the regionName
     */
    public String getRegionName() {
        return regionName;
    }

    /**
     * @param regionName the regionName to set
     */
    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    /**
     * @return the population
     */
    public long getPopulation() {
        return population;
    }

    /**
     * @param population the population to set
     */
    public void setPopulation(long population) {
        this.population = population;
    }

    /**
     * @return the caseCount
     */
    public long getCaseCount() {
        return caseCount;
    }

    /**
     * @param caseCount the caseCount to set
     */
    public void setCaseCount(long caseCount) {
        this.caseCount = caseCount;
    }
}
