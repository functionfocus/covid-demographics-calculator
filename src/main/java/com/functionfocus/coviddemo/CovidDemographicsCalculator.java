package com.functionfocus.coviddemo;

import com.functionfocus.coviddemo.sources.cases.ActiveCaseDataExtractor;
import com.functionfocus.coviddemo.sources.demographics.DemographicDataExtractor;
import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileSystemView;

/**
 *
 * @author matthew
 */
public class CovidDemographicsCalculator {
    
    public CovidDemographicsCalculator() {
        super();
    }
    
    private File demographicsFile;
    
    public void calculate() {
        
        if (this.getDemographicsFile() == null) {
            this.setDemographicsFile(this.promptForFileSelection("Select the demographics file"));
        }
        if (this.getDemographicsFile() != null) {
            RegionModel cases = new ActiveCaseDataExtractor().retrieve();
            RegionModel population = new DemographicDataExtractor(this.getDemographicsFile()).retrieve();
            System.out.println(String.format("Identified %d case records and %d population records", cases.getRegionNames().size(), population.getRegionNames().size()));
            cases.getRegionNames().forEach(caseRegionName -> {
                RegionDetailModel model = new RegionDetailModel();
                model.setRegionName(caseRegionName);
                model.setPopulation(population.getRegionCount(caseRegionName));
                model.setCaseCount(cases.getRegionCount(caseRegionName));
                System.out.println(String.format("%s has %d cases and a population of %d, with %s cases per 1000 people", caseRegionName, model.getCaseCount(), model.getPopulation(), model.getCaseCountRatioPer(1000)));
            });
            
        }
    }
    
    
    protected File promptForFileSelection(String dialogTitle) {
        System.setProperty("java.awt.headless", "false");
        File file;
        JFileChooser fileChooser = new JFileChooser("c:", FileSystemView.getFileSystemView());
        fileChooser.setDialogTitle(dialogTitle);
        fileChooser.showSaveDialog(null);
        file = fileChooser.getSelectedFile();
        return file;
    }

    /**
     * @return the demographicsFile
     */
    protected File getDemographicsFile() {
        return demographicsFile;
    }

    /**
     * @param demographicsFile the demographicsFile to set
     */
    protected void setDemographicsFile(File demographicsFile) {
        this.demographicsFile = demographicsFile;
    }
    
}
