package com.functionfocus.coviddemo;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author matthew
 */
public class RegionModel {

    private Map<String, Long> regionCountMap;

    public RegionModel() {

    }

    public void reportRegion(String regionName, long count) {
        if (regionName != null && !regionName.isEmpty()) {
            this.getRegionCountMap().put(regionName, count);
        }
    }

    public Set<String> getRegionNames(){
        return this.getRegionCountMap().keySet();
    }
    
    public long getRegionCount(String regionName) {
        return this.getRegionCountMap().getOrDefault(regionName, 0L);
    }

    /**
     * @return the regionCountMap
     */
    protected Map<String, Long> getRegionCountMap() {
        if (regionCountMap == null) {
            this.setRegionCountMap(new HashMap<>());
        }
        return regionCountMap;
    }

    /**
     * @param regionCountMap the regionCountMap to set
     */
    protected void setRegionCountMap(Map<String, Long> regionCountMap) {
        this.regionCountMap = regionCountMap;
    }

}
