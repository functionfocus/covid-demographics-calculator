package com.functionfocus.coviddemo.sources.demographics;

import java.io.File;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author matthew
 */
public class DemographicDataExtractorTest {

    protected static final String TEST_DATA_FILE_PATH = "src/test/resources/demographics-data/WPP2019_POP_F01_1_TOTAL_POPULATION_BOTH_SEXES.xlsx";

    public DemographicDataExtractorTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of init method, of class DemographicDataExtractor.
     */
    @Test
    public void testInit() {
        System.out.println("init");
        File file = new File(TEST_DATA_FILE_PATH);
        DemographicDataExtractor instance = new DemographicDataExtractor(file);
        instance.init();
        assertTrue(instance.getFile() != null);
        assertTrue(instance.getFile().isFile());
        assertTrue(instance.getWorkbook() != null);
    }

    /**
     * Test of retrieve method, of class DemographicDataExtractor.
     */
    @Test
    public void testRetrieve() {
        System.out.println("retrieve");
        DemographicDataExtractor instance = new DemographicDataExtractor(new File(TEST_DATA_FILE_PATH));
        instance.retrieve();
    }

    /**
     * Test of getFile method, of class DemographicDataExtractor.
     */
    @Test
    public void testGetFile() {
        System.out.println("getFile");
        File file = new File(TEST_DATA_FILE_PATH);
        DemographicDataExtractor instance = new DemographicDataExtractor(file);
        File expResult = file;
        File result = instance.getFile();
        assertEquals(expResult, result);
    }

    /**
     * Test of setFile method, of class DemographicDataExtractor.
     */
    @Test
    public void testSetFile() {
        System.out.println("setFile");
        File file = null;
        DemographicDataExtractor instance = new DemographicDataExtractor(new File(TEST_DATA_FILE_PATH));
        instance.setFile(file);
    }

    /**
     * Test of getWorkbook method, of class DemographicDataExtractor.
     */
    @Test
    public void testGetWorkbook() {
        System.out.println("getWorkbook");
        DemographicDataExtractor instance = new DemographicDataExtractor(new File(TEST_DATA_FILE_PATH));
        XSSFWorkbook result = instance.getWorkbook();
        assertTrue(result != null);
    }

    /**
     * Test of setWorkbook method, of class DemographicDataExtractor.
     */
    @Test
    public void testSetWorkbook() {
        System.out.println("setWorkbook");
        XSSFWorkbook workbook = null;
        DemographicDataExtractor instance = new DemographicDataExtractor(new File(TEST_DATA_FILE_PATH));
        instance.setWorkbook(workbook);
    }

}
