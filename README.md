# covid-demographics-calculator

This is a demonstration project that uses publicly-available data to demonstrate 
how a Java program can be used to perform analytical and reporting operations. 

## Data Source
The data used by this project is sourced from public sources:
 - https://population.un.org/wpp/Download/Standard/Population/
 - https://covid-19.dataflowkit.com/v1 (see https://www.programmableweb.com/api/dataflow-kit-covid-19-tracking)

## Disclaimer
This program source code is developed and provided as a demonstration of 
capabilities of the Java language and tools.  No warranty is made for fitness 
for purpose or accuracy of the calculations performed by this software.
 
